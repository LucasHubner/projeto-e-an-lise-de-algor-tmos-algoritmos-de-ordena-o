/* 
 * File:   SelectionSort.hpp
 * Author: Lucas
 *
 * Created on 16 de Outubro de 2014, 00:22
 */

#ifndef SELECTIONSORT_HPP
#define	SELECTIONSORT_HPP

#include "Custo.hpp"


class SelectionSort{
    Custo custo;
public:
    void Sort(int *vetor, int n);
    Custo getCusto(){
        return this->custo;
    }
};

void SelectionSort::Sort(int* vetor, int n){
    custo.reset();
    custo.start();
    int min,i,j;
    for(i = 0, custo.add(); custo.add() && i < n - 1; i++, custo.add()){
        min = i; custo.add();
        for(j = i+1,custo.add(2);custo.add() && j < n; j++, custo.add()){
            if(custo.add() && vetor[j] > vetor[min]){
                min = j;custo.add();
            }
        }
        swap(&vetor[i],&vetor[min]); custo.add(3);
    }
    custo.stop();
}
#endif	/* SELECTIONSORT_HPP */

