/* 
 * File:   main-teste.cpp
 * Author: lucas
 *
 * Created on 14 de Setembro de 2015, 21:44
 */

#include <cstdlib>
#include <fstream>
#include "Suport.h"
#include "Custo.hpp"
#include "BubbleSort.h"
#include "Suport.h"
#include "ReadFile.h"
#include "BubbleSortMelhorado.hpp"
#include "HeapSort.hpp"
#include "InsertionSort.hpp"
#include "MergeSort.hpp"
#include "QuickSort.hpp"
#include "RadixSort.hpp"
#include "SelectionSort.hpp"
#include <vector>
using namespace std;

int main(int argc, char** argv) {
    //Algoritmos
    BubbleSort bubblesort;
    BubbleSortMelhorado melhorado;
    HeapSort heapsort;
    InsertionSort insertionsort;
    MergeSort mergesort;
    QuickSort quicksort;
    RadixSort radixsort;
    SelectionSort selectionsort;
    
    //Leitura e escrita no arquivo
    FileReader read;
    
    //
    vector<string> *vetor = new vector<string>;
    vector<int> *vetorN = new vector<int>;
    int n;
    
    read.writeCabecalho();
    
    vetor = read.readFileToVectorS("files/arquivo.txt");
    
    for (int i = 0; i < vetor->size()-1; i++) {
        vetorN = read.readFileToVector(vetor->at(i));
        n = vetorN->size() - 1;
        int vetorOrigin[n];
        vectorToArray(vetorN, vetorOrigin);

        read.writeFileName(vetor->at(i));

        cout << "Tamanho do vetor :" << n << endl;
        cout << i << " de " << vetor->size()-1 << endl;
        cout << "Bubble" << endl;
        //Rodar BubbleSort
        bubblesort.Sort(vetorOrigin, n);
        read.writeCusto(bubblesort.getCusto());

        cout << "Bubble Melhorado" << endl;
        vectorToArray(vetorN, vetorOrigin);
        //Rodar BubbleSortMelhorado
        melhorado.Sort(vetorOrigin, n);
        read.writeCusto(melhorado.getCusto());

        cout << "Insertion" << endl;
        vectorToArray(vetorN, vetorOrigin);
        //Rodar InsertionSort
        insertionsort.Sort(vetorOrigin, n);
        read.writeCusto(insertionsort.getCusto());
        
        cout << "Selection" << endl;
        vectorToArray(vetorN, vetorOrigin);
        //Rodar SelectionSort
        selectionsort.Sort(vetorOrigin, n);
        read.writeCusto(selectionsort.getCusto());

        cout << "Quick" << endl;
        vectorToArray(vetorN, vetorOrigin);
        //Rodar QuickSort
        quicksort.Sort(vetorOrigin, n);
        read.writeCusto(quicksort.getCusto());

        cout << "Merge" << endl;
        vectorToArray(vetorN, vetorOrigin);
        //Rodar MergeSort
        mergesort.Sort(vetorOrigin, n);
        read.writeCusto(mergesort.getCusto());

        cout << "Heap" << endl;
        vectorToArray(vetorN, vetorOrigin);
        //Rodar HeapSort
        heapsort.Sort(vetorOrigin, n);
        read.writeCusto(heapsort.getCusto());

        cout << "Radix" << endl;
        vectorToArray(vetorN, vetorOrigin);
        //Rodar RadixSort
        radixsort.Sort(vetorOrigin, n);
        read.writeCusto(radixsort.getCusto());
        read.writeLine();
        delete vetorN;
    }
    return 0;
}

