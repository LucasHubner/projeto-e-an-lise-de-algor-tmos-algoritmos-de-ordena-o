/* 
 * File:   RadixSort.hpp
 * Author: lucas
 *
 * Created on 14 de Setembro de 2015, 21:29
 */

#ifndef RADIXSORT_HPP
#define	RADIXSORT_HPP


class RadixSort{
    Custo custo;
    
public:
    void Sort(int *vetor, int n);
    Custo getCusto(){
        return this->custo;
    }
};    

void RadixSort::Sort(int* a, int n){
    custo.reset();
    custo.start();
    int i, b[n];
    int m = a[0]; custo.add(1);
    int exp = 1; custo.add(1);

    for (i = 1, custo.add(1); custo.add(1) && (i < n) ; i++, custo.add(1)) {
        if (custo.add(1) && a[i] > m){
            m = a[i]; custo.add(1);
        }
    }

    while (custo.add(2) && m / exp > 0){
        int bucket[10] = {0}; custo.add(1);

        for (i = 0, custo.add(1); custo.add(2) && (i < n); i++, custo.add()){
            bucket[(a[i] / exp) % 10]++; custo.add(3); 
        }
        for (i = 1,custo.add(); custo.add() && (i < 10); i++, custo.add()){
            bucket[i] += bucket[i - 1]; custo.add(3);
        }
        for (i = n - 1,custo.add(2);custo.add() && (i >= 0); i--, custo.add()){
            b[--bucket[(a[i] / exp) % 10]] = a[i]; custo.add(4);
        }
        for (i = 0,custo.add();custo.add() && (i < n); i++, custo.add()){
            a[i] = b[i]; custo.add();
        }

        exp *= 10; custo.add(2);
    }
    custo.stop();
}

#endif	/* RADIXSORT_HPP */

