/* 
 * File:   Custo.hpp
 * Author: lucas
 *
 * Created on 23 de Outubro de 2014, 16:42
 */

#ifndef CUSTO_HPP
#define	CUSTO_HPP

#include <fstream>
#include <list>
#include <string>
#include <ctime>

class Custo{
    unsigned long long custo;
    clock_t inicio;
    clock_t fim;
    
public:
    Custo();
    void start();
    void stop();
    bool add(int);
    unsigned long long getCusto();
    float getTime();
    bool reset();
};

Custo::Custo(){
    custo = 0;
}

void Custo::start(){
   inicio = clock();
}

void Custo::stop(){
    fim = clock();
}

unsigned long long Custo::getCusto(){
    return custo;
}

float Custo::getTime(){
    return (fim-inicio)*1000/CLOCKS_PER_SEC;
}

bool Custo::add(int quantidade = 1){
    custo+= quantidade;
    return true;
}

bool Custo::reset(){
    custo = 0;
    inicio = 0;
    fim = 0;
    return true;
}


#endif	/* CUSTO_HPP */