/* 
 * File:   BubbleSort.h
 * Author: lucas
 *
 * Created on 11 de Setembro de 2015, 16:57
 */

#ifndef BUBBLESORT_H
#define	BUBBLESORT_H
#include "Custo.hpp"
#include "Suport.h"

class BubbleSort{
    Custo custo;
    
public:
    void Sort(int *vetor, int n);
    Custo getCusto(){
        return this->custo;
    }
};

void BubbleSort::Sort(int vetor[], int n){
    custo.reset();
    custo.start();
    int i, j;
    for(i = 0, custo.add(1);
            custo.add(1) && i < n-1 ;
            i++, custo.add(1)){
        
        for(j = 0, custo.add(1);
                 custo.add(1) && j < n-i;
                j++, custo.add(1)){   
            
            if( custo.add(2) && vetor[j]>vetor[j+1]){
                swap(&vetor[j],&vetor[j+1]); custo.add(4); // 3 do swap + 1 da conta i+1
            }
        }
    }
    custo.stop();
}


#endif	/* BUBBLESORT_H */

