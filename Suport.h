/* 
 * File:   Suport.h
 * Author: lucas
 *
 * Created on 23 de Outubro de 2014, 15:59
 */

#ifndef SUPORT_H
#define	SUPORT_H
#include <string>
#include <iostream>
#include <vector>
#include "Custo.hpp"

using namespace std;

void swap(int *x, int *y){
    int z = *x;
    *x = *y;
    *y = z;
}

void getRandomVector(int *vetor, int n){
    for(int i =0; i < n; i++){
        vetor[i] = rand();
    }
}
void copyVector(int *dest, int *source,int size){
    for(int i = 0; i < size; i++){
        dest[i]= source[i];
    }
}

void imprimeVetor(int *vetor, int n){
    for(int i = 0; i < n; i++){
        std::cout << i << " - " << vetor[i] << std::endl;
        //std::cout<< vetor[i] << std::endl;
    }
}

void imprimeCusto(Custo custo){
    cout << "Comparacoes/Atribuicoes - " << custo.getCusto() << endl;
    cout << "Tempo de Execucao       - " << custo.getTime() << endl;
}

void vectorToArray(vector<int> *vec, int *v){
    for(int i = 0; i < vec->size()-2; i++){
        v[i] = vec->at(i);
    }
}

#endif	/* SUPORT_H */

