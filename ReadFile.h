/* 
 * File:   ReadFile.h
 * Author: lucas
 *
 * Created on 15 de Setembro de 2015, 14:17
 */

#ifndef READFILE_H
#define	READFILE_H
#include <iostream>
#include <fstream>
#include <string>
#include <vector>

#include "Custo.hpp"

using namespace std;

class FileReader{    
    vector<int> *vetor;
    fstream Arquivo;
public:
    vector<int> * readFileToVector(string fileName){
        vector<int> *vetor = new vector<int>;
        string arquivo = "files/";
        arquivo += fileName.c_str();
        Arquivo.open(arquivo.c_str(), ios::in);
        Arquivo.seekg(0);
        if(Arquivo.is_open()){
            while(!Arquivo.eof()){
                int valor;
                Arquivo >> valor;
                vetor->push_back(valor);
            }
        }
        Arquivo.close();
        return vetor;
    }
    
    vector<string> * readFileToVectorS(string fileName){
        vector<string> *vetor = new vector<string>;
        Arquivo.open(fileName.c_str(), ios::in);
        Arquivo.seekg(0);
        if(Arquivo.is_open()){
            while(!Arquivo.eof()){
                string valor;
                Arquivo >> valor;
                vetor->push_back(valor);
            }
        }
        Arquivo.close();
        return vetor;
    }
    
    void writeCabecalho(){
        Arquivo.open("relatorio.txt", ios::out | ios::trunc);
        if (Arquivo.is_open()){
         string cabecalho;

         cabecalho = "\t \t \t \t \t \t \t \t Grupo: XXX, C++, Mili Segundos \n"
                 "\t BubbleSort\t \t "
                 "BubbleSort Melhorado\t \t"
                 "InsertionSort\t \t "
                 "SelectionSort\t \t "
                 "QuickSort\t \t "
                 "MergeSort\t \t "
                 "HeapSort\t \t "
                 "RadixSort \n"
                 
                 "\t Custo\t Tempo"
                 "\t Custo\t Tempo"
                 "\t Custo\t Tempo"
                 "\t Custo\t Tempo"
                 "\t Custo\t Tempo"
                 "\t Custo\t Tempo"
                 "\t Custo\t Tempo"
                 "\t Custo\t Tempo \n";
         
         Arquivo << cabecalho;
         Arquivo.close();
        }
        
    }
    void writeFileName(string fileName){
        Arquivo.open("relatorio.txt",ios::out | ios::app);
        if(Arquivo.is_open()){
            Arquivo << fileName << "\t";
        }
        Arquivo.close();
    }
    void writeCusto(Custo custo){
        Arquivo.open("relatorio.txt", ios::out | ios::app);
        if (Arquivo.is_open()){
            Arquivo << custo.getCusto() << "\t";
            Arquivo << custo.getTime() << "\t";
         Arquivo.close();
        }
    }
    void writeLine(){
        Arquivo.open("relatorio.txt", ios::out | ios::app);
        if (Arquivo.is_open()){
            Arquivo << endl;
         Arquivo.close();
        }
    }
};


#endif	/* READFILE_H */

