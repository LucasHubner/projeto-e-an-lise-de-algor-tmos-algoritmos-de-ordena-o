/* 
 * File:   MergeSort.hpp
 * Author: lucas
 *
 * Created on 14 de Setembro de 2015, 20:19
 */

#ifndef MERGESORT_HPP
#define	MERGESORT_HPP

class MergeSort{
    Custo custo;
    void Intercala(int, int, int, int []);
    void MergeSortAux(int,int,int[]);
public:
    void Sort(int *vetor, int n);
    Custo getCusto(){
        return this->custo;
    }
};

void MergeSort::Intercala(int p, int q, int r, int *v){
    int i,j,k;
    int w[r-p];//= (int*) malloc((r-p) *sizeof(int));
    i = p; custo.add(1);
    j = q; custo.add(1);
    k = 0; custo.add(1);
    //int *w[i-j];
    custo.add(1);
    
    while(custo.add(1) && i < q && 
            custo.add(1) && j < r){
        if(custo.add(1) && v[i] <= v[j]){
            w[k++] = v[i++]; custo.add(3);
        }
        else{
            w[k++] = v[j++]; custo.add(3);
        }
    }
    while(custo.add(1) && i < q){ 
        w[k++] = v[i++]; custo.add(3);
    }
    while(custo.add(1) && j < r){
        w[k++] = v[j++]; 
        custo.add(3);
    }
    for(i = p, custo.add(1);custo.add(1) && i<r; i++, custo.add(1)){ 
        v[i] = w[i-p]; custo.add(2);
    }
    
    //delete[] w;
    custo.add(1);
}

void MergeSort::MergeSortAux(int p, int r, int *v){
    if( custo.add(2) && p < r-1){
        int q = (p+r)/2; custo.add(3);
        MergeSortAux(p,q,v);
        MergeSortAux(q,r,v);
        Intercala(p,q,r,v);
    }
}

void MergeSort::Sort(int* vetor, int n){
    custo.reset();
    custo.start();
    MergeSortAux(0, n,vetor);
    custo.stop();
}

#endif	/* MERGESORT_HPP */

