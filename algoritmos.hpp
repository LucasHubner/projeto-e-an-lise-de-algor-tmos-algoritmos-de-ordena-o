/* 
 * File:   algoritmos.hpp
 * Author: lucas
 *
 * Created on 16 de Setembro de 2015, 03:35
 */

#ifndef ALGORITMOS_HPP
#define	ALGORITMOS_HPP

#include "Suport.h"
#include "Custo.hpp"
#include "BubbleSort.h"
#include "Suport.h"
#include "ReadFile.h"
#include "BubbleSortMelhorado.hpp"
#include "HeapSort.hpp"
#include "InsertionSort.hpp"
#include "MergeSort.hpp"
#include "QuickSort.hpp"
#include "RadixSort.hpp"
#include "SelectionSort.hpp"


#endif	/* ALGORITMOS_HPP */

