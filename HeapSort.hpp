/* 
 * File:   HeapSort.hpp
 * Author: lucas
 *
 * Created on 14 de Setembro de 2015, 20:28
 */

#ifndef HEAPSORT_HPP
#define	HEAPSORT_HPP

class HeapSort{
    Custo custo;
    void MaxHeapfy(int*, int,int);
public:
    void Sort(int *vetor, int n);
    Custo getCusto(){
        return this->custo;
    }
};    

void HeapSort::MaxHeapfy(int *v, int i, int n){
    int maior = i; custo.add(1);
    int left = i*2+1; custo.add(3);// = * +
    int right = i*2+2; custo.add(3);

    if(custo.add(1) && left < n && 
       custo.add(1) && v[i] < v[left]){
        maior = left; custo.add(1);
    }
    if(custo.add(1) && right < n && 
       custo.add(1) && v[i] < v[right] ){
        maior = right; custo.add(1);
    }
    if(custo.add(1) && i != maior){
        swap(&v[i],&v[maior]); custo.add(3);
        MaxHeapfy(v, maior, n);
    }
}

void HeapSort::Sort(int* v, int n){
    custo.reset();
    custo.start();
    int i;
    for (i = (n-1/2), custo.add(3) ; 
            custo.add(1) && (i >= 0);
            i--, custo.add(1)){
        MaxHeapfy(v,i,n);
    }
    do{
        swap(&v[0],&v[--n]); custo.add(4);// 3 do swap + 1 --
        MaxHeapfy(v,0,n);
    }
    while( custo.add(1) && n > 0);
    
    custo.stop();
}

#endif	/* HEAPSORT_HPP */

