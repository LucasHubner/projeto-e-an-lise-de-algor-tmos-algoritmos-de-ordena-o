/* 
 * File:   BubbleSortMelhorado.hpp
 * Author: lucas
 *
 * Created on 14 de Setembro de 2015, 20:06
 */

#ifndef BUBBLESORTMELHORADO_HPP
#define	BUBBLESORTMELHORADO_HPP

#include "Custo.hpp"


class BubbleSortMelhorado{
    Custo custo;
    
public:
    void Sort(int *vetor, int n);
    Custo getCusto(){
        return this->custo;
    }
};

void BubbleSortMelhorado::Sort(int* vetor, int n){
    custo.reset();
    custo.start();
    int i, j;
    int troca = 1; custo.add(1);
    for(j = n-1, custo.add(2);
            custo.add(1) && j > 0 && 
            custo.add(1) && troca;
            
            j--, custo.add(1)){
        troca = 0; custo.add(1);
        for(i = 0, custo.add(1);
                custo.add(1) && i < j;
                i++, custo.add(1)){   
            
            if( custo.add(2) && vetor[i]>vetor[i+1]){
                swap(&vetor[i],&vetor[i+1]); custo.add(4); // 3 do swap + 1 da conta;
                troca = 1; custo.add(1);
            }
        }
    }
    custo.stop();
}

#endif	/* BUBBLESORTMELHORADO_HPP */

