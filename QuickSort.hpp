/* 
 * File:   QuickSort.hpp
 * Author: lucas
 *
 * Created on 14 de Setembro de 2015, 20:12
 */

#ifndef QUICKSORT_HPP
#define	QUICKSORT_HPP

class QuickSort{
private:
    Custo custo;
    int Partition(int*, int, int);
    void QuickSortAux(int*,int,int);
    
public:
    Custo getCusto(){
        return this->custo;
    }
    
    void Sort(int *vetor, int n);    
    
};

void QuickSort::Sort(int* vetor, int n){
    custo.reset();
    custo.start();
    QuickSortAux(vetor, 0, n);
    custo.stop();
}

void QuickSort::QuickSortAux(int *a, int l, int r){
    int j;
    if(custo.add(1) && l < r){
        j = Partition (a, l ,r ); custo.add(1);
        QuickSortAux(a, l, j-1);
        QuickSortAux(a, j+1, r);
    }
}
int QuickSort::Partition(int *a, int l, int r){
    int pivot,t,i,j;
    pivot = a[l]; custo.add(1);
    i = l;
    j = r+1;
    while(1){
        do {
            i++; custo.add(1);
        }while(custo.add(1) && pivot > a[i] && 
                custo.add(1) && i <= r);
        do {
            j--; custo.add(1);
        }while(custo.add(1) &&  pivot < a[j]);
        if(custo.add(1) && i >= j) break;
        swap(&a[i],&a[j]); custo.add(3);
    }
    swap(&a[l], &a[j]); custo.add(3);
    
    custo.add(1); //Conta return
    return j;
}



#endif	/* QUICKSORT_HPP */

