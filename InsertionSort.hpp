/* 
 * File:   InsertionSort.hpp
 * Author: Lucas
 *
 * Created on 15 de Outubro de 2014, 22:44
 */

#ifndef INSERTIONSORT_HPP
#define	INSERTIONSORT_HPP

#include "Custo.hpp"


class InsertionSort{
    Custo custo;
public:
    void Sort(int *vetor, int n);
    Custo getCusto(){
        return this->custo;
    }
};

void InsertionSort::Sort(int* vetor, int n){
    custo.reset();
    custo.start();
    int i,j,key;
    for(j = 1, custo.add(1);
            custo.add(1) && j < n ;
            j++, custo.add(1)){
        key = vetor[j]; custo.add(1);
        i = j-1; custo.add(2);
        
        while(custo.add(1) &&i >= 0 &&
              custo.add(1) && vetor[i] > key){
            vetor[i+1] = vetor[i]; custo.add(2);
            i--; custo.add(1);
        }
        vetor[i+1] = key; custo.add(2);
    }
    custo.stop();
}

#endif	/* INSERTIONSORT_HPP */

